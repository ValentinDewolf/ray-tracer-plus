CC = g++
CXXFLAGS =-std=c++14 -pedantic -Wall -Wextra -O0 -g3

INCLUDE = -I "src" \
		  -I "src/objects" \
		  -I "src/render" \
		  -I "src/lights"

CXXFLAGS+=$(INCLUDE)

S_SRC = parser.cc \
		scene.cc \
		vector3.cc \
		ascii.cc \
		main.cc

S_RENDER = color.cc \
		   rendered.cc \
		   screen.cc

S_OBJECTS = object.cc \
			camera.cc \
			sphere.cc \
			plane.cc

S_LIGHTS = light.cc \
		   ambient.cc \
		   directional.cc \
		   point.cc


SRC = $(addprefix src/, $(S_SRC)) \
	  $(addprefix src/render/, $(S_RENDER)) \
	  $(addprefix src/objects/, $(S_OBJECTS)) \
	  $(addprefix src/lights/, $(S_LIGHTS)) \


OBJ = ${SRC:.cc=.o}
BIN = rtp

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CXXFLAGS) $(OBJ) -o $(BIN)

clean:
	rm -rvf $(OBJ) $(BIN)

check: $(BIN)
	./tests/suite.sh

ascii: $(BIN)
	./tests/suite.sh --ascii

test: $(BIN)
	./$(BIN) tests/inputs/double_spheres.in picture.ppm
	feh picture.ppm


plane: $(BIN)
	./$(BIN) tests/inputs/plane1.in picture.ppm
	feh picture.ppm


remake: clean all

.PHONY: all clean
