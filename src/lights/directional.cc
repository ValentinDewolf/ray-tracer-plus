#include "directional.hh"

Directional::Directional(Color color, Vector3 direction)
    : Light(color), dir_{direction}
{}


const Vector3& Directional::dir_get() const
{
    return dir_;
}

void Directional::dir_set(const Vector3& value)
{
    dir_ = value;
}



Color Directional::compute_value(const Rendered& obj_render, const Vector3& pos,
                                 const Object& obj)
{
    const Vector3& normal = obj.normal_vect(pos);

    double diff = obj_render.diff_get() * normal.dot_product(dir_.normalize());

    double r = obj_render.color_get().r_get() * color_.r_get() * diff;
    double g = obj_render.color_get().g_get() * color_.g_get() * diff;
    double b = obj_render.color_get().b_get() * color_.b_get() * diff;

    Color res(r, g, b);
    res.positive();
    return res;
}

