#pragma once
#include "vector3.hh"
#include "color.hh"
#include "object.hh"

class Light
{
public:
    Light(Color color)
        : color_{color}
    {}

    ~Light() = default;


    virtual Color compute_value(const Rendered& obj_render, const Vector3& pos,
                                const Object& obj) = 0;

    Color color_get() const;

    void color_set(const Color& value);

protected:
    Color color_;
};
