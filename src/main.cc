#include <iostream>
#include <fstream>
#include "parser.hh"
#include "screen.hh"
#include "ascii.hh"


void print_to_ppm(char* path, Screen& screen)
{
    std::ofstream file(path);
    file << "P3" << std::endl;
    file << screen.width_get() << " " << screen.height_get() << std::endl;
    file << "255" << std::endl;

    for (auto line : screen.pixels_get())
    {
        for (auto pix : line)
            file << pix << " ";
        file << std::endl;
    }
}



int main(int argc, char* argv[])
{
    bool ascii = false;
    int opt_index = 1;

    if (std::string(argv[1]) == "--ascii")
    {
        ascii = true;
        opt_index += 1;
    }

    if (argc < 3)
    {
        std::cerr << "Please enter an input and output file" << std::endl;
        return 1;
    }

    std::ifstream input(argv[opt_index]);

    if (!input.is_open())
    {
        std::cerr << "ERROR: Could not open file: " << argv[opt_index] << std::endl;
        return 2;
    }

    Parser psr(input);
    try
    {
        Scene& scene = psr.parse_scene();

        if (ascii)
        {
            //Resize screen for ascii terminal display
            scene.screen_get()->width_set(100);
            scene.screen_get()->height_set(60);

            scene.compute();
            Ascii converter(*scene.screen_get());
            converter.print_screen();
        }
        else
        {
            scene.compute();
            print_to_ppm(argv[opt_index + 1], *scene.screen_get());
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << "Execption: "<< e.what() << std::endl;
        return 3;
    }

    return 0;
}
