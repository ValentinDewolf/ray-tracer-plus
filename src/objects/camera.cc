#include "camera.hh"


Camera::Camera(Vector3 pos, Vector3 u, Vector3 v)
    : Object(pos), u_(u), v_(v)
{}


Vector3 Camera::u_get() const
{
    return u_;
}

Vector3 Camera::v_get() const
{
    return v_;
}


Rendered* Camera::intersect(const Ray& ray, double& distance)
{
    distance = 0;
    (void)ray;
    return nullptr;
}



Vector3 Camera::normal_vect(const Vector3& pos) const
{
    (void)pos;
    return Vector3(0, 0, 0);
}
