#pragma once

#include "vector3.hh"
#include "ray.hh"
#include "rendered.hh"

class Object
{
public:
    Object(Vector3 pos);
    ~Object() = default;

    const Vector3& pos_get();

    /* returns the rendered of the intersected object by ray
     * if there is an intersection, updates the distance given as argument */
    virtual Rendered* intersect(const Ray& ray, double& distance) = 0;

    virtual Vector3 normal_vect(const Vector3& pos) const = 0;

protected:
    Vector3 pos_;
};
