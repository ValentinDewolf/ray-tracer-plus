#pragma once

#include "color.hh"
#include "vector3.hh"
#include "rendered.hh"
#include "object.hh"

class Plane : public Object, public Rendered
{
public:
    Plane(double a, double b, double c, double d, Vector3 pos, double diff,
          double refl, double spec, double shin, Color color);

    ~Plane() = default;


    virtual Rendered* intersect(const Ray& ray, double& distance) override;

    virtual Vector3 normal_vect(const Vector3& pos) const override;


private:
    double a_;
    double b_;
    double c_;
    double d_;
};
