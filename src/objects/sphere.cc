#include <cmath>
#include "sphere.hh"

Sphere::Sphere(double radius, Vector3 pos, double diff, double refl,
               double spec, double shin, Color color)
    : Object(pos), Rendered(diff, refl, spec, shin, color), radius_{radius}
{}



Rendered* Sphere::intersect(const Ray& ray, double& distance)
{
    Vector3 origin(0, 0, 0);
    double a = ray.dir.dot_product(ray.dir);
    double b = (ray.dir * 2).dot_product(pos_.compute(ray.pos));
    double c = std::pow(pos_.compute(ray.pos).distance(origin), 2) - std::pow(radius_, 2);

    double delta = (b * b) - (4 * a * c);

    if (delta < 0) // no intersection point
        return nullptr;


    if (delta > 0) // two intersection points
    {
        double x1 = (-1 * b - std::sqrt(delta)) / (2 * a);
        double x2 = (-1 * b + std::sqrt(delta)) / (2 * a);
        distance = (x1 > x2) ? x2 : x1;
    }
    else // one intersection point
        distance = (-1 * b) / (1 * a);


    return dynamic_cast<Rendered*>(this);
}


Vector3 Sphere::normal_vect(const Vector3& pos) const
{
    return pos.compute(pos_).normalize();
}
