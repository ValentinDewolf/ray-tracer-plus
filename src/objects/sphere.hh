#pragma once
#include "color.hh"
#include "vector3.hh"
#include "rendered.hh"
#include "object.hh"

class Sphere : public Object, public Rendered
{
public:
    Sphere(double radius, Vector3 pos, double diff, double refl, double spec,
           double shin, Color color);

    ~Sphere() = default;


    virtual Rendered* intersect(const Ray& ray, double& distance) override;

    virtual Vector3 normal_vect(const Vector3& pos) const override;


private:
    double radius_;
};
