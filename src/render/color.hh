#pragma once
#include <iostream>

class Color
{
public:
    Color();
    Color(double r, double g, double b);
    Color(double r, double g, double b, double refr, double opac);

    ~Color() = default;

    double r_get() const;
    double g_get() const;
    double b_get() const;

    void r_set(double value);
    void g_set(double value);
    void b_set(double value);

    void positive();

    Color& operator=(const Color& other);

    Color operator+(const Color& other) const;

private:
    double r_;
    double g_;
    double b_;
    double refr_;
    double opac_;
};



std::ostream& operator<<(std::ostream& out, const Color& c);
