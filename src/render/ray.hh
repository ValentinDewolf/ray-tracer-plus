#pragma once

#include "vector3.hh"

struct Ray
{
    Ray(Vector3 pos, Vector3 dir)
        : pos{pos}, dir{dir}
    {}

    Vector3 pos;
    Vector3 dir;
};
