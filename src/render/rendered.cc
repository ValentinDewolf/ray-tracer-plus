#include "rendered.hh"

Rendered::Rendered(double diff, double refl, double spec,
                   double shin, Color color)
    : diff_{diff}, refl_{refl}, spec_{spec}, shin_{shin}, color_{color}
{}
