#pragma once
#include <vector>
#include "color.hh"

class Screen
{
public:
    using color_matrix = std::vector<std::vector<Color>>;

    Screen(unsigned width, unsigned height);
    ~Screen();

    unsigned width_get() const;
    unsigned height_get() const;

    void width_set(unsigned value);
    void height_set(unsigned value);

    color_matrix& pixels_get();

    void set_pixel(unsigned x, unsigned y, const Color& color) const;

private:
    unsigned width_;
    unsigned height_;

    color_matrix* pixels_;
};
