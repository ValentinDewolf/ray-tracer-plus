#include <cmath>
#include "vector3.hh"


Vector3::Vector3 (double x, double y, double z)
    : x_(x), y_(y), z_(z)
{}

Vector3::~Vector3 ()
{}

double Vector3::x_get() const
{
    return x_;
}

void Vector3::x_set(double value)
{
    x_ = value;
}

double Vector3::y_get() const
{
    return y_;
}

void Vector3::y_set(double value)
{
    y_ = value;
}

double Vector3::z_get() const
{
    return z_;
}

void Vector3::z_set(double value)
{
    z_ = value;
}


double Vector3::distance(const Vector3& other) const
{
    return std::sqrt(std::pow(x_ - other.x_, 2) + std::pow(y_ - other.y_, 2) +
                     std::pow(z_ - other.z_, 2));
}


double Vector3::dot_product(const Vector3& other) const
{
    return x_ * other.x_ + y_ * other.y_ + z_ * other.z_;
}


Vector3 Vector3::operator+(const Vector3& other) const
{
    Vector3 res(x_ + other.x_, y_ + other.y_, z_ + other.z_);
    return res;
}



Vector3 Vector3::operator*(double x) const
{
    Vector3 res(x_ * x, y_ * x, z_ * x);
    return res;
}

Vector3 Vector3::operator*(const Vector3& other) const
{
    double u = y_ * other.z_ - z_ * other.y_;
    double v = z_ * other.x_ - x_ * other.z_;
    double w = x_ * other.y_ - y_ * other.x_;
    return Vector3(u, v, w);
}


Vector3 Vector3::normalize() const
{
    double denominator = std::sqrt(std::pow(x_, 2) + std::pow(y_, 2) +
                                   std::pow(z_, 2));
    return Vector3(x_ / denominator, y_ / denominator, z_ / denominator);
}


Vector3 Vector3::compute(const Vector3& other) const
{
    Vector3 res(other.x_ - x_, other.y_ - y_, other.z_ - z_);
    return res;
}


std::ostream& operator<<(std::ostream& out, const Vector3& v)
{
    out << "(" << v.x_get() << ", " << v.y_get() << ", " << v.z_get() << ")";
    return out;
}
