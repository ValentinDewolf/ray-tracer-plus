#! /bin/sh

bin=./rtp

i=1

for file in tests/inputs/*.in; do
    echo "Creating file $i from $file"
    if [ "$1" = "--ascii" ]; then
        $bin "--ascii" $file "tests/image$i.ppm"
    else
        $bin $file "tests/image$i.ppm"
    fi
    i=$((i + 1))
done

if [ "$1" != "--ascii" ]; then
    feh tests/*.ppm
fi
